
require('dotenv').load();
// Include the cluster module
/*var cluster = require('cluster');

// Code to run if we're in the master process
if (cluster.isMaster) {

    // Count the machine's CPUs
    var cpuCount = require('os').cpus().length;

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    // Listen for terminating workers
    cluster.on('exit', function (worker) {

        // Replace the terminated workers
        console.log('Worker ' + worker.id + ' died :(');
        cluster.fork();

    });

// Code to run if we're in a worker process
} else {*/

    const AWS = require('aws-sdk');
    const express = require('express');
    const bodyParser = require('body-parser');
    const path = require('path');
    const logger = require('morgan');
    const cors = require('cors');
    var util = require('util');
    var expressValidator = require('express-validator');
    var app = express();

    const db = require('./database/mysqlDB').db;

    db.initDb(false).then(function(result){}).catch(function(err){ console.log("err in db init, err = ", util.inspect(err))});

    var server = require('http').Server(app);
    var io = require('socket.io')(server);

    var port = process.env.PORT || 3001;
    server.listen(port);
    console.log("server listens on "+port);

    //controllers
    //sockets controllers
    var mainSocketsController = require('./sockets/mainSocketsController').connectionHandler(io);

    //todo new index below
    var index = require('./controllers/index');
    const gamesController = require('./controllers/gamesController');
    const authController = require('./controllers/authController');

    AWS.config.region = process.env.REGION;
    app.set('view engine', 'ejs');
    app.set('views', __dirname + '/views');

    app.use(logger('dev'));
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(bodyParser.json({limit: '50mb'}));
   /* app.use(bodyParser.urlencoded({
        extended: true
    }));*/

    app.use(cors());

    app.use(expressValidator());

    //controllers below
    app.use('/', index);
    app.use('/games', gamesController);
    app.use('/auth', authController);
    // error handlers below

    // development error handler
    // will print stacktrace
    if (app.get('env') === 'development') {
        app.use(function(err, req, res, next) {
            console.log("err at end of app.js in development mode, err = "+util.inspect(err));
            res.status(err.status || 500).json({
                error : err.error
            });
        });
    }

    // production error handler
    // no stacktraces leaked to user
    app.use(function(err, req, res, next) {
        console.log("err at end of app.js in production mode, err = "+util.inspect(err));
        res.status(err.status || 500).json({
            error : err.error
        });
    });
//}