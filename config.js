module.exports = {
    pathToFilesDirectory : __dirname+'/files',
    linkToGameFile : process.env.SERVER_URL+'/games/getGameFile?fileName=',
    linkToGameThumbnail : process.env.SERVER_URL+'/games/getGameThumbnail?thumbnailName='
};