var express = require('express');
var router = express.Router();
const fs = require('fs');
const util = require('util');
const async = require('async');
const shortId = require('shortid');
const config = require('../config');
const filesManager = require('./extraModules/filesManager');
const usersRepo = require('../database/repositories/usersRepo');

//todo works
router.post('/login', function (req,res,next) {

    var authData = {
        email : req.body.email,
        password : req.body.password
    };

    usersRepo.validateLogin(authData.email, authData.password, function(err, user){
        if(err){
            return next({status : 400, error : err});
        }
        res.json({
            id : user.id,
            role : user.role
        });
    });
});

router.post('/userRegistration', function(req,res,next){
    if(req.body.email&&req.body.password){

        var newUser = {
            email : req.body.email,
            password : req.body.password,
            role : 'user'
        };

        usersRepo.create(newUser, function(err, newUser){
            if(err){
                return next({status : 400, error : err});
            } else {
                res.json({
                    newUser : newUser
                });
            }
        });
    } else {
        return next({status : 400, error : 'not all required fields have been sent'});
    }
});

module.exports = router;