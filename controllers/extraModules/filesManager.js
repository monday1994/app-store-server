const fs = require('fs');
const async = require('async');
const config = require('./../../config');
const util = require('util');

const uploadFile = function(file, pathToFile){
    return new Promise(function(resolve,reject){
        var base64Data = file.split(',')[1];
        fs.writeFile(pathToFile, base64Data, 'base64', function(err){
            if(err){
                reject(err);
            } else {
                resolve(true);
            }
        });
    });
};

exports.uploadFile = uploadFile;

const uploadImage = function(buffer, pathToFile){
    return new Promise(function(resolve,reject){
        fs.writeFile(pathToFile, buffer, 'base64', function(err){
            if(err){
                reject(err);
            } else {
                resolve(true);
            }
        });
    });
};

exports.uploadImage = uploadImage;


exports.removeFileByLink = function(link){
    const pathToFilesDirectory = config.pathToFilesDirectory;
    return new Promise(function(resolve,reject){

        var data = link.split('?')[1];

        var fileName = data.split('=')[1].split('&')[0];

        var userId = data.split('&')[1].split('=')[1];

        var pathToFile = pathToFilesDirectory+'/'+userId+'/'+fileName;
        console.log("path to file = "+util.inspect(pathToFile));
        checkFileExists(pathToFile).then(function(doesFileExist){
            if(doesFileExist){
                console.log("file exists ! ");
                fs.unlink(pathToFile, function(err){
                    if(err){
                        reject(err);
                    } else {
                        resolve(true);
                    }
                });
            } else {
                reject('file with path : '+pathToFile+' does not exist');
            }
        });
    });
};


const getFileExtensionFromFileName = function(fileName){

    var extension = fileName.split('.')[1];

    var ext = extension.toLowerCase();

    return '.'+ext;
};

exports.getFileExtensionFromFileName = getFileExtensionFromFileName;

function checkFileExists(filepath){
    return new Promise(function(resolve, reject){
        fs.access(filepath, fs.F_OK, function(error){
            resolve(!error);
        });
    });
}

exports.checkFileExists = checkFileExists;

function getFileExtensionFromBase64(base64File){
    return base64File.split(';')[1].split('.')[1];
}

exports.getFileExtensionFromBase64 = getFileExtensionFromBase64;
/*


exports.doesFileExists = checkFileExists;

const removeFiles = function(linksArr){
    const pathToFilesDirectory = config.pathToFilesDirectory;
    return new Promise((resolve,reject) => {
        async.each(linksArr, (currentLink, cb) => {

        let data = currentLink.split('?')[1];

    let fileName = data.split('=')[1].split('&')[0];

    let userId = data.split('&')[1].split('=')[1];

    let pathToFile = pathToFilesDirectory+'/'+userId+'/'+fileName;
    console.log("path to file = "+util.inspect(pathToFile));
    checkFileExists(pathToFile).then(doesFileExist => {
        if(doesFileExist){
            console.log("file exists ! ");
            fs.unlink(pathToFile, (err) => {
                if(err){
                    return cb(err);
                }
                cb(null);
        });
        } else {
            cb(null);
}
}).catch(err => {
        return cb(err);
});
}, (err, result) => {
        if (err) {
            reject(err);
        }
        resolve(result);
    });
});
};

exports.removeFiles = removeFiles;




const getFileTypeByExtension = function(extension){

    const videoExtensionsArr = ['.ogv', '.avi', '.3gp', '.flv', '.webm', '.mp4'];
    const imageExtensionsArr = ['.jpg', '.png', '.jpeg', '.tiff'];

    let idx = videoExtensionsArr.indexOf(extension);

    if(idx !== -1){
        return 'video';
    } else {
        let idx = imageExtensionsArr.indexOf(extension);

        if(idx !== -1){
            return 'image';
        }
    }
};

exports.getFileTypeByExtension = getFileTypeByExtension;

*/
