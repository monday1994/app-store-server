const util = require('util');
const imageMagick = require('imagemagick');
const async = require('async');

exports.imageMagickResize = function(buffer, xDimension, yDimension, callback){

    async.waterfall([
        function(callback){
            imageMagick.resize({
                srcData : buffer,
                width : xDimension,
                height : yDimension,
                format: 'jpg',
                quality : 1,
                sharpening : 0.2
            }, function(err, stdout, stderr){
                if(err) return callback(err);
                console.log("in first resize");
                callback(null, stdout);
            });
        },
        function(resizedImage, callback){
            console.log("before crop");
            imageMagick.crop({
                srcData : resizedImage,
                format: 'jpg',
                width : xDimension,
                height : yDimension,
                quality : 1,
                sharpening : 0.7,
                gravity: "Center"
            }, function(err, stdout, stderr){
                if(err) return callback(err);
                callback(null, stdout);
            });
        }
    ], function(err, fullyResizedImage){
        if(err){
            return callback(err);
        }
        var convertedToBuffer = new Buffer(fullyResizedImage, 'binary');
        callback(null, convertedToBuffer);
    });
};
