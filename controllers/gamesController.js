var express = require('express');
var router = express.Router();
const fs = require('fs');
const util = require('util');
const async = require('async');
const shortId = require('shortid');
const config = require('../config');
const filesManager = require('./extraModules/filesManager');
const gamesRepo = require('../database/repositories/gamesRepo');
const imageProcessor = require('./extraModules/imageProcessor');

//todo works
router.get('/fetchAllGames', function(req,res,next){

    gamesRepo.getAll(function(err, games){
        if(err){
            return next({status : 400, error : err});
        }
        res.json({
            games : games
        });
    });
});

//todo works
router.get('/fetchAllUserGames', function(req,res,next){

    var userId = req.query.id;

    gamesRepo.getByUserId(userId, function(err, games){
        if(err){
            return next({status : 400, error : err});
        }
        res.json({
            games : games
        });
    });
});

//todo works
router.post('/createGame', function (req,res,next) {

    if(req.body.name&&req.body.description&&req.body.version&&req.body.thumbnail&&req.body.game){
        var newGame = {
            name : req.body.name,
            description : req.body.description,
            version : req.body.version,
            thumbnail : req.body.thumbnail,
            game : req.body.game
        };

        var pathToFilesDirectory = config.pathToFilesDirectory;
        var thumbnailFileExtension = 'jpg';//filesManager.getFileExtensionFromBase64(newGame.thumbnail);

        var gameFileName = shortId.generate();
        var gameThumbnailName = shortId.generate();

        var linkToGameFile = config.linkToGameFile+gameFileName+'.exe';
        var linkToGameThumbnail = config.linkToGameThumbnail+gameThumbnailName+'.'+thumbnailFileExtension;

        var fullPathToGameFile = pathToFilesDirectory+'/'+gameFileName+'.exe';
        var fullPathToThumbnail = pathToFilesDirectory+'/'+gameThumbnailName+'.'+thumbnailFileExtension;

        async.waterfall([
            function(callback){
                filesManager.uploadFile(newGame.game, fullPathToGameFile).then(function(result){
                    newGame.game=linkToGameFile;
                    callback(null);
                }).catch(function (err) {
                    return callback(err);
                });
            },
            function(callback){
                var buffer = new Buffer(newGame.thumbnail.split(',')[1], 'base64');
                filesManager.uploadImage(buffer, fullPathToThumbnail).then(function(result){
                    newGame.thumbnail=linkToGameThumbnail;
                    callback(null);
                }).catch(function (err) {
                    return callback(err);
                });
            },
            function(callback){
                gamesRepo.create(newGame, function(err, createdGame){
                    if(err){
                        return callback(err);
                    }
                    callback(null, createdGame);
                });
            }
        ], function(err, createdGame){
            if(err){
                return next({status : 400, error: err});
            }
            res.json({
                createdGame : createdGame
            });
        });
    } else {
        return next({status : 400, err : 'body does not contain all required fields'});
    }
});

//todo works
router.get('/getGameThumbnail', function(req,res,next){
    var gameThumbnailName = req.query.thumbnailName;
    var pathToFile = config.pathToFilesDirectory+'/'+gameThumbnailName;
    res.sendFile(pathToFile);
});

//todo works
router.get('/getGameFile', function(req,res,next){
    var fileName = req.query.fileName;
    var pathToFile = config.pathToFilesDirectory+'/'+fileName;
    res.sendFile(pathToFile);
});



router.post('/editGame', function (req,res,next) {

    if(req.body.name&&req.body.description&&req.body.version&&req.body.thumbnail&&req.body.game){

        var gameToBeEdit = {
            id : req.body.id,
            name : req.body.name,
            description : req.body.description,
            version : req.body.version,
            thumbnail : req.body.thumbnail,
            game : req.body.game
        };

        console.log("body = "+util.inspect(req.body));

        var pathToFilesDirectory = config.pathToFilesDirectory;

        var thumbnailFileExtension = null;

        if(gameToBeEdit.thumbnail !== ';base64,'){
            thumbnailFileExtension = filesManager.getFileExtensionFromBase64(gameToBeEdit.thumbnail);
        }

        var gameFileName = shortId.generate();
        var gameThumbnailName = shortId.generate();

        var linkToGameFile = config.linkToGameFile+gameFileName+'.exe';
        var linkToGameThumbnail = config.linkToGameThumbnail+gameThumbnailName+'.'+thumbnailFileExtension;

        var fullPathToGameFile = pathToFilesDirectory+'/'+gameFileName+'.exe';
        var fullPathToThumbnail = pathToFilesDirectory+'/'+gameThumbnailName+'.'+thumbnailFileExtension;

        async.waterfall([
            function(callback){
                if(gameToBeEdit.game !== ';base64,'){
                    console.log("uploading game");
                    filesManager.uploadFile(gameToBeEdit.game, fullPathToGameFile).then(function(result){
                        gameToBeEdit.game=linkToGameFile;
                        callback(null);
                    }).catch(function (err) {
                        return callback(err);
                    });
                } else {
                    delete gameToBeEdit.game;
                    callback(null);
                }

            },
            function(callback){
                if(gameToBeEdit.thumbnail !== ';base64,'){
                    console.log("uploading thumbnail");
                    filesManager.uploadFile(gameToBeEdit.thumbnail, fullPathToThumbnail).then(function(result){
                        gameToBeEdit.thumbnail=linkToGameThumbnail;
                        callback(null);
                    }).catch(function (err) {
                        return callback(err);
                    });
                } else {
                    delete gameToBeEdit.thumbnail;
                    callback(null);
                }
            },
            function(callback){
                gamesRepo.edit(gameToBeEdit, function(err, updatedGame){
                    if(err){
                        return callback(err);
                    }
                    callback(null, updatedGame);
                });
            }
        ], function(err, updatedGame){
            if(err){
                return next({status : 400, error: err});
            }
            res.json({
                updatedGame : updatedGame
            });
        });
    } else {
        return next({status : 400, err : 'body does not contain all required fields'});
    }

});

router.post('/removeGame', function (req,res,next) {

    var gameId = req.body.gameId;

    if(gameId){
        gamesRepo.remove(gameId, function(err, result){
            if(err){
                return next({ status : 400, error : err});
            }
            res.json({
                result : true
            });
        });
    } else {
        return next({
            status : 400,
            error : 'gameId is required'
        });
    }
});

router.post('/assignDownloadedGameToUser', function(req,res,next){
    var userId = req.body.userId;
    var gameId = req.body.gameId;

    gamesRepo.assignGameToUser(userId, gameId, function(err, result){
        if(err){
            return next({status : 400, error : err});
        }

        res.json({
            message : 'game has been assigned'
        });
    })
});

router.post('/removeDownloadedGameFromUser', function(req,res,next){
    var userId = req.body.userId;
    var gameId = req.body.gameId;

    gamesRepo.removeGameFromUser(userId, gameId, function(err, result){
        if(err){
            return next({status : 400, error : err});
        }

        res.json({
            message : 'assignment has been removed'
        });
    })
});

module.exports = router;