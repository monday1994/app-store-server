var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('main.ejs');
});

router.get('/sockets', function(req,res,next){
    res.render('index.ejs');
});

router.get('/chat', function(req,res,next){
    res.render('chat.ejs');
});

router.get('/test', function (req,res,next) {
    require('../database/repositories/gamesRepo').getAllUsersAssociatedWithGame(3, function(err, result){
        res.json(result);
    })
})

module.exports = router;