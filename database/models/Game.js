module.exports = function(sequelize, DataTypes) {
    const Game = sequelize.define("Game", {
        name: {
            type : DataTypes.STRING
        },
        description : {
            type : DataTypes.STRING
        },
        version : {
            type : DataTypes.STRING
        },
        thumbnail : {
            type : DataTypes.STRING
        },
        game : {
            type : DataTypes.STRING
        }
    }, {
        timestamps: false
    });

    return Game;
};