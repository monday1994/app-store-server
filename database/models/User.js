module.exports = function(sequelize, DataTypes) {
    const User = sequelize.define("User", {
        email: {
            type : DataTypes.STRING,
            unique : true
        },
        password : {
            type : DataTypes.STRING
        },
        role : {
            type : DataTypes.ENUM('admin', 'user'),
            defaultValue : 'user'
        }
    }, {
        timestamps: false
    });

    return User;
};