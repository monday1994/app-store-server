const util = require('util');
const Promise = require('bluebird');
const Sequelize = require('sequelize');

//Sequelize.useCLS(transactionsNamespace);
//operators
const Op = Sequelize.Op;


console.log("db name = "+process.env.DB_NAME);
console.log("db username = "+ process.env.DB_USERNAME);
console.log("db pass = "+ process.env.DB_PASS);
console.log("host = " + process.env.DB_HOST);

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER_NAME, process.env.DB_PASS, {
    dialect : "mysql",
    host: 'localhost',
    port: 3306
}, {operatorAliases : false});


// Connect all the models/tables in the database to a db object,
//so everything is accessible via one object
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

const initEntities = function(){

//Models/tables
    db.Game = require('./models/Game')(sequelize, Sequelize);
    db.User = require('./models/User')(sequelize, Sequelize);

    return new Promise((resolve, reject) => {
        //creating relations
        Promise.all([
            db.User.belongsToMany(db.Game, {as : 'Games', through : {model : 'GameUser', unique : false}, foreignKey : 'UserId'}),
            db.Game.belongsToMany(db.User, {as : 'Users', through : {model : 'GameUser', unique : false}, foreignKey : 'GameId'})
    ]).then((results)=>{
        //console.log("results db association = "+util.inspect(results));
        console.log("db has been initialized");
    resolve(results);
}).catch(err => {
        console.error('db error = '+err);
    reject(err);
});
});
};

db.initDb = function(isForced){
    return new Promise((resolve, reject) => {

        // db.sequelize.drop().then(result => {
        initEntities().then((results)=>{
        db.sequelize.sync({force : isForced}).then(() => {
        console.log("db initialized");
    resolve('database initialized');
}).catch(err => {
        console.error(err);
    reject(err);
});
}).catch(err=>{
        console.log("error occured when entities were initializing");
    reject(err);
});
    /*}).catch(err => {
        reject(err);
    });*/

});
};

module.exports = {
    db : db,
    sequelize : sequelize,
    Sequelize : Sequelize
};