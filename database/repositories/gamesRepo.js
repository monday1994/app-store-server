const util = require('util');
const db = require('../mysqlDB').db;
const mainSocketsController = require('../../sockets/mainSocketsController');
const async = require('async');


exports.create = function(newGame, callback){
    db.Game.create(newGame).then(function(createdGame){
        callback(null, createdGame);
    }).catch(function(err){
        return callback(err);
    });
};

exports.edit = function(gameToBeEdit, callback){

    var gameToBeEditVersion = gameToBeEdit.version;
    var sendNotifFlag = false;
    var gameId = gameToBeEdit.id;
    db.Game.findOne({where : {id : gameToBeEdit.id}}).then(function(gameFromDb){
        if(gameFromDb){

            if(gameFromDb.dataValues.version != gameToBeEditVersion){
                sendNotifFlag = true;
            }

            if(gameToBeEdit.thumbnail === ';base64,'){
                gameToBeEdit.thumbnail = gameFromDb.thumbnail;
            }

            if(gameToBeEdit.game === ';base64,'){
                gameToBeEdit.game = gameFromDb.game;
            }

            gameFromDb.update(gameToBeEdit).then(function(updatedGame){
                console.log("after update");
                if(sendNotifFlag){
                    console.log("send flag ");
                    getAllUsersAssociatedWithGame(gameId, function(err, game){
                        if(err){
                            console.log("err in get all users = ", util.inspect(err));
                            return callback(err);
                        }
                        console.log("game with associated users = ", util.inspect(game));
                        async.each(game.Users, function(currentUser, cb){
                            console.log("sending notification");
                            console.log("current user = ", util.inspect(currentUser));
                            mainSocketsController.sendNotification(gameToBeEdit.id, currentUser.id);
                            cb(null);
                        }, function (err) {
                           if(err){
                               return callback(err);
                           }
                           callback(null, updatedGame);
                        });
                    });
                } else {
                    console.log("no send flag");
                    callback(null, updatedGame);
                }
            }).catch(function(err){
                return callback(err);
            });
        } else {
            console.log("in first game does not exist");
            return callback('game with id '+gameToBeEditVersion.id+' does not exist in db');
        }
    }).catch(function(err){
        return callback(err);
    });
};

exports.getById = function(id, callback){
    db.Game.findOne({where : {id : id}}).then(function(gameFromDb){
        if(gameFromDb){
            callback(null, gameFromDb);
        } else {
            return callback('game with id '+id+' does not exist in db')
        }
    }).catch(function(err){
        return callback(err);
    });
};

exports.getByUserId = function(userId, callback){
    db.Game.findAll({ include : [
            {
                model : db.User, as : 'Users', where : {id : userId}
            }
        ]}).then(function(gamesFromDb){
        callback(null, gamesFromDb);
    }).catch(function(err){
        return callback(err);
    });
};

const getAllUsersAssociatedWithGame = function(gameId, callback){
    db.Game.findOne({where : {id : gameId}, include : [
            {
                model : db.User, as : 'Users'
            }
        ]}).then(function(gameFromDb){
        if(gameFromDb){
            callback(null, gameFromDb.get({plain : true}));
        } else {
            return callback('game with id '+gameId+' does not exist in db')
        }
    }).catch(function(err){
        return callback(err);
    });
};

exports.getAllUsersAssociatedWithGame = getAllUsersAssociatedWithGame;

exports.getAll = function(callback){
    db.Game.findAll().then(function(games){
        callback(null, games);
    }).catch(function(err){
        return callback(err);
    });
};

exports.remove = function(id, callback){
    db.Game.destroy({where : { id : id}}).then(function(result){
        callback(null, result);
    }).catch(function(err){
        return callback(err);
    });
};

exports.assignGameToUser = function(userId, gameId, callback){

    db.Game.findOne({where : {id : gameId}}).then(function(gameFromDb){
        if(gameFromDb){
            db.User.findOne({where : { id : userId}}).then(function(userFromDb){
                if(userFromDb){
                    console.log("sr f = ", util.inspect(userFromDb));
                    userFromDb.addGames(gameFromDb).then(function(result){
                        callback(null, true);
                    }).catch(function(err){
                        return callback(err);
                    });
                } else {
                    return callback('user with id : '+userId+' does not exist in db');
                }
            }).catch(function(err){
                return callback(err);
            });
        } else {
            return callback('game with id : '+gameId+' does not exist in db');
        }
    }).catch(function(err){
        return callback(err);
    });
};

exports.removeGameFromUser = function(userId, gameId, callback){

    db.Game.findOne({where : {id : gameId}}).then(function(gameFromDb){
        if(gameFromDb){
            db.User.findOne({where : { id : userId}}).then(function(userFromDb){
                if(userFromDb){
                    gameFromDb.removeUsers(userFromDb).then(function(result){
                        callback(null, true);
                    }).catch(function(err){
                        return callback(err);
                    });
                } else {
                    return callback('user with id : '+userId+' does not exist in db');
                }
            }).catch(function(err){
                return callback(err);
            });
        } else {
            return callback('game with id : '+gameId+' does not exist in db');
        }
    }).catch(function(err){
        return callback(err);
    });
};

