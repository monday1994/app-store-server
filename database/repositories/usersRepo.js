const util = require('util');
const db = require('../mysqlDB').db;

exports.create = function(newUser, callback){
    db.User.create(newUser).then(function(createdUser){
        callback(null, createdUser);
    }).catch(function(err){
        return callback(err);
    });
};

exports.edit = function(userToBeEdit, callback){
    db.User.findOne({where : {id : userToBeEdit.id}}).then(function(userFromDb){
        if(userFromDb){
            userFromDb.update(userToBeEdit).then(function(updatedUser){
                callback(null, updatedUser);
            }).catch(function(err){
                return callback(err);
            });
        } else {
            return callback('user with id '+id+' does not exist in db');
        }
    }).catch(function(err){
        return callback(err);
    });
};

exports.validateLogin = function(email, password, callback){
    db.User.findOne({where : {email : email, password : password}}).then(function (userFromDb) {
        console.log("user from db = ", util.inspect(userFromDb));
        if(userFromDb){
            callback(null, userFromDb);
        } else {
            return callback('login data incorrect');
        }
    }).catch(function(err) {
        return callback(err);
    });
};

exports.getById = function(id, callback){
    db.User.findOne({where : {id : id}}).then(function(userFromDb){
        if(userFromDb){
            callback(null, userFromDb);
        } else {
            return callback('user with id '+id+' does not exist in db')
        }
    }).catch(function(err){
        return callback(err);
    });
};

exports.getAll = function(callback){
    db.User.findAll().then(function(users){
        callback(null, users);
    }).catch(function(err){
        return callback(err);
    });
};

exports.remove = function(id, callback){
    db.User.destroy({where : { id : id}}).then(function(result){
        callback(null, result);
    }).catch(function(err){
        return callback(err);
    });
};
