var util = require('util');
var app = require('express');
var router = app.Router();

var connectedUsers = {};


//todo ERROR EVENT
// exception -> returns { error : err }


exports.connectionHandler = function(io) {

    io.on('connection', function (socket) {
        console.log("on connection");

        if(socket.handshake.query.id){
            var userId = socket.handshake.query.id;

            connectedUsers[userId] = socket;
            console.log("user " + userId + " has been connected");
        }


        socket.on('registerSocket', function(data){

            console.log("connected users length before register = ", Object.keys(connectedUsers).length);
            connectedUsers[data.id] = socket;
            console.log("connected users length after register = ", Object.keys(connectedUsers).length);
            console.log("socket registered");
        });

        socket.on('deleteSocket', function(data){

            console.log("connected users length before delete = ", Object.keys(connectedUsers).length);
            delete connectedUsers[data.id];
            console.log("connected users length after delete = ", Object.keys(connectedUsers).length);
            console.log("socket deleted");
        });

        socket.on('message', function(data){

            console.log("message = ", util.inspect(data));
             if(connectedUsers[data.targetUserId]){
                 console.log("message has been sent");
                 connectedUsers[data.targetUserId].emit('message', data);
             } else {
                 socket.emit('exception', {error : 'targeted user is not connected'});
             }
        });



        // function should be invoked always when internet connection is turned off
        socket.on("disconnect", function () {
            console.log("user disconnected");

            if(socket.handshake.query.id){
                console.log("user "+socket.handshake.query.id+" disconnected");
                delete connectedUsers[socket.handshake.query.id];
            }
        });

    });
};

exports.sendNotification = function(gameId, userId){
    if(connectedUsers[userId]){
        connectedUsers[userId].emit('update', {
            gameId : gameId
        });
    }
};

exports.connectedUsers = connectedUsers;

